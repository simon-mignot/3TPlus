(function()
{
    angular.module('3TPlus', []);
    
    angular.module('3TPlus').controller('MainController', MainControllerFunction);
    
    MainControllerFunction.$inject = ['$scope', '$http'];
    function MainControllerFunction($scope, $http)
    {
        $scope.team = 'square'; // pawn, expand
        $scope.ghostPawn = {x: -1, y: -1};
        $scope.expandTool = { x: -2, y: -2, valid: false };
        $scope.game = {};
        $scope.me = {};
        
        $scope.titleElement = document.getElementsByTagName('title')[0];
        
        $scope.defaults = { expandTool: { x: -2, y: -2, valid: false }, ghostPawn: {x: -1, y: -1} };
        $scope.pass = '';
        
        $scope.init = function()
        {
            setTimeout(update, 500);
        };
        $scope.tryReset = function(type)
        {
            $http.get('/reset/' + type).then(function(res) { }, function(err) { console.log(err); });
            if(type === 'players')
                $scope.me = {};
        };
        $scope.tryEnterGame = function()
        {
            $http.get('/getPawn/' + $scope.username).then(function(res)
            {
                if(res.status === 200)
                    $scope.me = res.data;
            }, function(err) { console.log(err); });
        };
        
        $scope.mouseOut = function()
        {
            console.log('mouseout');
            $scope.expandTool = Object.assign({}, $scope.defaults.expandTool);
        };
        $scope.mouseOver = function(x, y)
        {
            if($scope.game.grid[y][x] === -1)
            {
                $scope.ghostPawn = {x: x, y: y};
            }
            else
                $scope.ghostPawn = Object.assign({}, $scope.defaults.ghostPawn);
            
            if($scope.game.grid[y][x] === -2)
            {
                $scope.expandTool.x = x;
                $scope.expandTool.y = y;
                $scope.expandTool.valid = isValidExpand(x, y);
            }
            else
                $scope.expandTool = Object.assign({}, $scope.defaults.expandTool);
        };
        $scope.mouseClick = function(x, y)
        {
            let action = getActionType(x, y);
            console.log(x, y);
            if(action === 'pawn' && isValidPawn(x, y) || action === 'expand' && isValidExpand(x, y))
            {
                $http.post('/play/' + action + '/' + x + '/' + y + '/' + $scope.me.id).then(function(res)
                {
                    console.log(res);
                }, function(err)
                {
                    console.log(err);
                });
            }
        };
        
        $scope.getExpandClass = function(x, y)
        {
            if($scope.expandTool.x < 0 || $scope.expandTool.y < 0)
                return '';
            if(x > $scope.expandTool.x - 2 && x < $scope.expandTool.x + 2
                && y > $scope.expandTool.y - 2 && y < $scope.expandTool.y + 2)
                return isValidExpand($scope.expandTool.x, $scope.expandTool.y) ? 'expand-valid' : 'expand-invalid';
            return '';
        };
        
        $scope.resetActive = function()
        {
            $scope.lastActivity = Date.now();
        };
        
        $scope.isWinningPawn = function(x, y)
        {
            for(let i = 0; i < $scope.game.winLine.length; ++i)
                if($scope.game.winLine[i].x === x && $scope.game.winLine[i].y === y)
                    return true;
            return false;
        };
        
        function update()
        {
            $http.get('/update/' + $scope.me.id + '/' + $scope.lastActivity).then(function(res)
            {
                $scope.game = res.data;
                let me = $scope.game.players[$scope.me.id] || null;
                if(me === null || me.name !== $scope.me.name || me.id !== $scope.me.id)
                    $scope.me = {};
                if(me && me.id === $scope.game.currentPlayerID)
                    updateTitle('It\'s your turn !');
                else
                    updateTitle();
                setTimeout(update, 500);
            }, function(err)
            {
                console.log(err);
            });
        }
        
        function updateTitle(toAppend)
        {
            console.log($scope.titleElement);
            if(typeof(toAppend) !== 'undefined')
            {
                toAppend = ' - ' + toAppend;
                //setTimeout(updateTitle, 250);
            }
            else
                toAppend = '';
            let nb = $scope.game.players.length;
            $scope.titleElement.textContent = '3TPlus (' + nb + ' player' + (nb > 1 ? 's' : '') + ')' + toAppend;
            
        }
        
        function getActionType(x, y)
        {
            if(cellExists(x, y) && $scope.game.grid[y][x] === -1)
                return 'pawn';
            if(cellExists(x, y) && $scope.game.grid[y][x] === -2)
                return 'expand';
            return 'none';
        }
        function isValidPawn(x, y)
        {
            return cellExists(x, y) && $scope.game.grid[y][x] === -1;
        }
        function isValidExpand(x, y)
        {
            console.log('a');
            // Check inner different than undiscovered (-2)
            for(let j = y - 1; j < y + 2; ++j)
                for(let i = x - 1; i < x + 2; ++i)
                    if(cellExists(i, j) && $scope.game.grid[j][i] !== -2)
                        return false;
            // Check outer with at least one different than undiscovered (-2)
            for(let i = x - 2; i < x + 3; ++i)
                if((cellExists(i, y - 2) && $scope.game.grid[y - 2][i] !== -2)
                    || (cellExists(i, y + 2) && $scope.game.grid[y + 2][i] !== -2))
                    return true;
            for(let j = y - 1; j < y + 1; ++j)
                if((cellExists(x - 2, j) && $scope.game.grid[j][x - 2] !== -2)
                    || (cellExists(x + 2, j) && $scope.game.grid[j][x + 2] !== -2))
                    return true;
            return false;
        }
        
        function cellExists(x, y)
        {
            return typeof($scope.game.grid[y]) === 'object' && typeof($scope.game.grid[y][x]) === 'number';
        }
    }
})();