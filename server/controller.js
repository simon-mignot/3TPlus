let gridHelper = require('./Grid.js')();

let currentReset = 0;
let game =
    {
        grid: [],
        pawnAvailable: ['cross', 'circle', 'square', 'triangle'],
        players: [],
        currentPlayerID: 0,
        lastPawn: {x: -1, y: -1},
        lastBoardChange: {},
        winLine: []
    };

// Start game
setInterval(updateIndicator, 5000);
initGame();


// Game actions
function initGame()
{
    game.grid = gridHelper.Grid();
    for(let i = 0; i < 10; ++i)
        gridHelper.updateMargin(game.grid);
}
function resetGame()
{
    ++currentReset;
    initGame();
    for(let i = 0; i < game.players.length; ++i)
    {
        game.players[i].pawns = 0;
        game.players[i].expands = 0;
    }
    game.lastBoardChange = {top: -1, bottom: -1, right: -1, left: -1};
    game.lastPawn = {x: -1, y: -1};
    game.currentPlayerID = currentReset % game.players.length || 0;
    game.winLine = [];
}
function resetPlayers()
{
    game.players = [];
}
function nextPlayer()
{
    game.currentPlayerID++;
    game.currentPlayerID %= game.players.length;
}

function updateIndicator()
{
    for(let i = 0; i < game.players.length; ++i)
    {
        let now = Date.now();
        let networkInactiveTime = now - game.players[i].lastNetworkActivity;
        if(networkInactiveTime > 60000)
            game.players[i].networkIndicator = 2;
        else if(networkInactiveTime > 20000)
            game.players[i].networkIndicator = 1;
        else
            game.players[i].networkIndicator = 0;
    
        
        let inactiveTime = now - game.players[i].lastActivity;
        if(inactiveTime > 60000)
            game.players[i].activityIndicator = 2;
        else if(inactiveTime > 20000)
            game.players[i].activityIndicator = 1;
        else
            game.players[i].activityIndicator = 0;
    }
}

// Actions
function placePawn(x, y, team)
{
    console.log('try placing pawn at ', x, y);
    if(gridHelper.at(game.grid, x, y) === -1)
        game.grid[y][x] = game.currentPlayerID;
    else
        return false;
    
    game.winLine = hasWinLine();
    if(game.winLine)
    {
        game.currentPlayerID = -1;
        console.log('win');
        return true;
    }
    nextPlayer();
    return true;
}
function expand(x, y)
{
    console.log('try expand from ', x, y);
    if(!expandValid(x, y))
        return false;
    for(let j = y - 1; j < y + 2; ++j)
        for(let i = x - 1; i < x + 2; ++i)
            game.grid[j][i] = -1;
    game.lastBoardChange = gridHelper.updateMargin(game.grid);
    nextPlayer();
    return true;
}


// Checks
function expandValid(x, y)
{
    // Check inner different than undiscovered (-2)
    for(let j = y - 1; j < y + 2; ++j)
        for(let i = x - 1; i < x + 2; ++i)
            if(!gridHelper.cellExists(game.grid, i, j) || game.grid[j][i] !== -2)
                return false;
    // Check outer with at least one different than undiscovered (-2)
    for(let i = x - 2; i < x + 3; ++i)
        if((gridHelper.at(game.grid, i, y - 2) !== -2)
            || (gridHelper.at(game.grid, i, y + 2) !== -2))
            return true;
    for(let j = y - 1; j < y + 1; ++j)
        if((gridHelper.at(game.grid, x - 2, j) !== -2)
            || (gridHelper.at(game.grid, x + 2, j) !== -2))
            return true;
    return false;
}

function hasWinLine()
{
    let w = gridHelper.getWidth(game.grid),
        h = gridHelper.getHeight(game.grid);
    for(let y = 0; y < h; ++y)
    {
        for(let x = 0; x < w; ++x)
        {
            let cell = gridHelper.at(game.grid, x, y);
            if(cell < 0 || cell > game.players.length)
                continue;
    
            let i;
            // horizontal
            for(i = 1; i < 4; ++i)
                if(gridHelper.at(game.grid, x + i, y) !== cell)
                    break;
            if(i === 4)
                return [{x: x, y: y}, {x: x + 1, y: y}, {x: x + 2, y: y}, {x: x + 3, y: y}];
    
            // vertical
            for(i = 1; i < 4; ++i)
                if(gridHelper.at(game.grid, x, y + i) !== cell)
                    break;
            if(i === 4)
                return [{x: x, y: y}, {x: x, y: y + 1}, {x: x, y: y + 2}, {x: x, y: y + 3}];
    
            // diagonal bottom-left/top-right
            for(i = 1; i < 4; ++i)
                if(gridHelper.at(game.grid, x + i, y - i) !== cell)
                    break;
            if(i === 4)
                return [{x: x, y: y}, {x: x + 1, y: y - 1}, {x: x + 2, y: y - 2}, {x: x + 3, y: y - 3}];
    
            // diagonal top-left/bottom-right
            for(i = 1; i < 4; ++i)
                if(gridHelper.at(game.grid, x + i, y + i) !== cell)
                    break;
            if(i === 4)
                return [{x: x, y: y}, {x: x + 1, y: y + 1}, {x: x + 2, y: y + 2}, {x: x + 3, y: y + 3}];
        }
    }
}


module.exports = function()
{
    return {
        update: function(req, res)
        {
            let playerID = req.params.playerID;
            if(playerID >= 0 && playerID < game.players.length)
            {
                game.players[playerID].lastNetworkActivity = Date.now();
                game.players[playerID].lastActivity = req.params.lastActivity;
            }
            res.status(200).json(game);
        },
        getPawn: function(req, res)
        {
            if(game.players.length < game.pawnAvailable.length)
            {
                let id = game.players.length;
                let pawn = game.pawnAvailable[id];
                let name = req.params.name;
                let player =
                {
                    name: name,
                    pawn: pawn,
                    id: id,
                    pawns: 0,
                    expands: 0,
                    activityIndicator: -1,
                    networkIndicator: -1
                };
                game.players.push(player);
                res.status(200).json(player);
                console.log('Gave ' + pawn + ' to #' + id + ' ' + name + '.');
            }
            else
                res.status(403).end();
        },
        play: function(req, res)
        {
            let x = parseInt(req.params.x);
            let y = parseInt(req.params.y);
            let playerID = parseInt(req.params.playerID);
            if(playerID !== game.currentPlayerID || playerID < 0 || playerID >= game.players.length)
            {
                res.status(403).end();
                return;
            }
            game.lastPawn = { x: -1, y: -1 };
            if(req.params.tool === 'pawn')
            {
                if(placePawn(x, y, playerID))
                {
                    ++game.players[playerID].pawns;
                    game.lastPawn = { x: x, y: y };
                    res.status(200).end();
                }
                else
                    res.status(403).end();
            }
            else if(req.params.tool === 'expand')
            {
                if(expand(x, y))
                {
                    ++game.players[playerID].expands;
                    game.lastPawn = { x: x + game.lastBoardChange.left, y: y + game.lastBoardChange.top };
                    res.status(200).end();
                }
                else
                    res.status(403).end();
            }
            else
                res.status(404).end();
        },
        reset: function(req, res)
        {
            if(req.params.type === 'players')
                resetPlayers();
            if(req.params.type === 'game' || req.params.type === 'players')
                resetGame();
            else
            {
                res.status(403).end();
                return;
            }
            console.log(game);
            res.status(200).end();
        }
    };
};