let express = require('express');
let app = express();


app.use('/node_modules/', express.static(__dirname + '/node_modules'));
app.use(express.static(__dirname + '/client'));

let controller = require('./server/controller.js')();
app.get('/getPawn/:name', controller.getPawn);
app.get('/update/:playerID/:lastActivity', controller.update);
app.post('/play/:tool/:x/:y/:playerID', controller.play);
app.get('/reset/:type', controller.reset);


app.listen(process.env.PORT, function()
{
    console.log('Server started on ' + process.env.PORT + ' !');
});